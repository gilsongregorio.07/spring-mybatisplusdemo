package com.bib.mybatisplusdemo.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bib.mybatisplusdemo.MybatisplusdemoApplication;
import com.bib.mybatisplusdemo.model.Book;
import com.bib.mybatisplusdemo.model.BookQueryCriteria;
import com.bib.mybatisplusdemo.service.BookService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.WebApplicationContext;
import utils.ResponseUtils;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static utils.BookUtils.*;
import static utils.MockMvcUtils.asJsonString;
import static utils.MockMvcUtils.performRequest;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {MybatisplusdemoApplication.class})
@DisplayName("Assets Controller Testing")
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BookControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookService bookService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void before() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    private String REQUEST_MAPPING_URL = "/api/books";
    private Book VALID_BOOK = generateValidBooks();
    private Book INVALID_BOOK = generateInvalidBooks();
    private List<Book> VALID_BOOK_LIST = generateListOfBooks();

    @Nested
    @DisplayName("Get Books Method Tests")
    class GetBooksTests{
        @Test
        @Order(1)
        public void getBooks_Return200Status_IfSuccess() throws Exception {
            BookQueryCriteria bookQueryCriteria = new BookQueryCriteria();

            Page page = new Page<>();
            page.setRecords(VALID_BOOK_LIST);
            when(bookService.getBooks(bookQueryCriteria)).thenReturn(ResponseEntity.ok(page));

            MvcResult result = performRequest(mockMvc,bookQueryCriteria, REQUEST_MAPPING_URL + "/list", HttpMethod.GET);

            assertEquals(200,result.getResponse().getStatus());
            assertEquals(asJsonString(page),result.getResponse().getContentAsString());
        }

        @Test
        @Order(2)
        public void getBooks_throwHttpRequestMethodNotSupportedException_IfUsedWrongHttpMethods() throws Exception {
            BookQueryCriteria bookQueryCriteria = new BookQueryCriteria();

            MvcResult result = performRequest(mockMvc,bookQueryCriteria, REQUEST_MAPPING_URL + "/list", HttpMethod.POST);

            assertTrue(result.getResolvedException() instanceof HttpRequestMethodNotSupportedException);
            assertEquals("Request method 'POST' not supported", result.getResolvedException().getMessage());
            assertEquals(HttpStatus.METHOD_NOT_ALLOWED.value(), result.getResponse().getStatus());
        }

        @Test
        @Order(3)
        public void getBooks_throwHttpMessageNotReadableException_IfRequestBodyIsWrong() throws Exception {
            MvcResult result = performRequest(mockMvc,"username: test", REQUEST_MAPPING_URL + "/list", HttpMethod.GET);

            assertTrue(result.getResolvedException() instanceof HttpMessageNotReadableException);
            assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());
        }

    }

    @Nested
    @DisplayName("Insert Book Method Tests")
    class InsertBookTests{
        @Test
        @Order(4)
        public void insertBooks_Return200Status_IfSuccess() throws Exception {

            when(bookService.addBook(VALID_BOOK)).thenReturn(ResponseEntity.ok(VALID_BOOK));

            MvcResult result = performRequest(mockMvc,VALID_BOOK, REQUEST_MAPPING_URL + "/save", HttpMethod.POST);

            assertEquals(200,result.getResponse().getStatus());
        }

        @Test
        @Order(5)
        public void insertBooks_ReturnErrorMessage_IfSavingInvalidBooks() throws Exception {

            when(bookService.addBook(INVALID_BOOK)).thenReturn(ResponseEntity.ok(INVALID_BOOK));

            MvcResult result = performRequest(mockMvc,INVALID_BOOK, REQUEST_MAPPING_URL + "/save", HttpMethod.POST);

            String stringResponse = result.getResponse().getContentAsString();
            Map<String, Object> mapResponse = ResponseUtils.convertStringResponseToMap(stringResponse);

            assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());
            assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException);
            assertThat(mapResponse.get("authorName").toString()).isNotEmpty();
            assertThat(mapResponse.get("price").toString()).isNotEmpty();
            assertThat(mapResponse.get("genre").toString()).isNotEmpty();
            assertThat(mapResponse.get("name").toString()).isNotEmpty();
        }

        @Test
        @Order(6)
        public void insertBooks_throwHttpRequestMethodNotSupportedException_IfUsedWrongHttpMethods() throws Exception {

            MvcResult result = performRequest(mockMvc,VALID_BOOK, REQUEST_MAPPING_URL + "/save", HttpMethod.GET);

            assertTrue(result.getResolvedException() instanceof HttpRequestMethodNotSupportedException);
            assertEquals("Request method 'GET' not supported", result.getResolvedException().getMessage());
            assertEquals(HttpStatus.METHOD_NOT_ALLOWED.value(), result.getResponse().getStatus());
        }

        @Test
        @Order(7)
        public void insertBooks_throwHttpMessageNotReadableException_IfRequestBodyIsWrong() throws Exception {
            MvcResult result = performRequest(mockMvc,"'price': 300.30", REQUEST_MAPPING_URL + "/save", HttpMethod.POST);

            assertTrue(result.getResolvedException() instanceof HttpMessageNotReadableException);
            assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());
        }
    }

    @Nested
    @DisplayName("Update Book Method Tests")
    class UpdateBookTests{
        @Test
        @Order(8)
        public void updateBooks_Return200Status_IfSuccess() throws Exception {

            when(bookService.updateBook(VALID_BOOK,14L)).thenReturn(ResponseEntity.ok(VALID_BOOK));

            MvcResult result = performRequest(mockMvc,VALID_BOOK, REQUEST_MAPPING_URL + "/update/14", HttpMethod.PUT);

            assertEquals(200,result.getResponse().getStatus());
        }

        @Test
        @Order(9)
        public void updateBooks_ReturnErrorMessage_IfUpdatingInvalidBooks() throws Exception {

            when(bookService.updateBook(INVALID_BOOK,14L)).thenReturn(ResponseEntity.ok(INVALID_BOOK));

            MvcResult result = performRequest(mockMvc,INVALID_BOOK, REQUEST_MAPPING_URL + "/update/14", HttpMethod.PUT);

            String stringResponse = result.getResponse().getContentAsString();
            Map<String, Object> mapResponse = ResponseUtils.convertStringResponseToMap(stringResponse);

            assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());
            assertTrue(result.getResolvedException() instanceof MethodArgumentNotValidException);
            assertThat(mapResponse.get("authorName").toString()).isNotEmpty();
            assertThat(mapResponse.get("price").toString()).isNotEmpty();
            assertThat(mapResponse.get("genre").toString()).isNotEmpty();
            assertThat(mapResponse.get("name").toString()).isNotEmpty();
        }

        @Test
        @Order(10)
        public void updateBooks_throwHttpRequestMethodNotSupportedException_IfUsedWrongHttpMethods() throws Exception {

            MvcResult result = performRequest(mockMvc,VALID_BOOK, REQUEST_MAPPING_URL + "/update/14", HttpMethod.POST);

            assertTrue(result.getResolvedException() instanceof HttpRequestMethodNotSupportedException);
            assertEquals("Request method 'POST' not supported", result.getResolvedException().getMessage());
            assertEquals(HttpStatus.METHOD_NOT_ALLOWED.value(), result.getResponse().getStatus());
        }

        @Test
        @Order(11)
        public void updateBooks_throwHttpMessageNotReadableException_IfRequestBodyIsWrong() throws Exception {
            MvcResult result = performRequest(mockMvc,"'price': 300.30", REQUEST_MAPPING_URL + "/update/14", HttpMethod.PUT);

            assertTrue(result.getResolvedException() instanceof HttpMessageNotReadableException);
            assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());
        }
    }

    @Nested
    @DisplayName("Delete Book Method Tests")
    class DeleteBookTests{
        @Test
        @Order(12)
        public void deleteBooks_Return200Status_IfSuccess() throws Exception {

            when(bookService.deleteBookById(14L)).thenReturn(ResponseEntity.ok("Book with id [1] has been deleted"));

            MvcResult result = performRequest(mockMvc,null, REQUEST_MAPPING_URL + "/delete/1", HttpMethod.DELETE);

            assertEquals(200,result.getResponse().getStatus());
        }

        @Test
        @Order(13)
        public void deleteBooks_throwHttpRequestMethodNotSupportedException_IfUsedWrongHttpMethods() throws Exception {

            MvcResult result = performRequest(mockMvc,null, REQUEST_MAPPING_URL + "/delete/1", HttpMethod.POST);

            assertTrue(result.getResolvedException() instanceof HttpRequestMethodNotSupportedException);
            assertEquals("Request method 'POST' not supported", result.getResolvedException().getMessage());
            assertEquals(HttpStatus.METHOD_NOT_ALLOWED.value(), result.getResponse().getStatus());
        }
    }
}

