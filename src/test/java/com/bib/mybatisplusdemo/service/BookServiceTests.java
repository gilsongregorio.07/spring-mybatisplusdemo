package com.bib.mybatisplusdemo.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bib.mybatisplusdemo.advice.BookAlreadyExistsException;
import com.bib.mybatisplusdemo.advice.BookDoesNotExistException;
import com.bib.mybatisplusdemo.mapper.BookMapper;
import com.bib.mybatisplusdemo.model.Book;
import com.bib.mybatisplusdemo.model.BookQueryCriteria;
import com.bib.mybatisplusdemo.service.impl.BookServiceImpl;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static utils.BookUtils.*;

@ExtendWith(MockitoExtension.class)
@TestMethodOrder(MethodOrderer.MethodName.class)
public class BookServiceTests {

    @Mock
    private BookMapper bookMapper;

    @InjectMocks
    private BookServiceImpl bookService;

    private Book VALID_BOOK = generateValidBooks();
    private Book INVALID_BOOK = generateInvalidBooks();
    private List<Book> VALID_BOOK_LIST = generateListOfBooks();

    @Nested
    @DisplayName("Get Books Method Tests")
    class GetBooksTests{
        @Test
        public void getBooks_ReturnPageOfBooks_IfSuccessInGettingAllTheBooks(){



            doReturn(VALID_BOOK_LIST).when(bookMapper).selectList(any(Wrapper.class));

            BookQueryCriteria bookQueryCriteria = new BookQueryCriteria();
            ResponseEntity<Page<Book>> empList = bookService.getBooks(bookQueryCriteria);

            assertEquals(2, empList.getBody().getRecords().size());
            assertEquals(HttpStatus.OK.value(),empList.getStatusCode().value());
            verify(bookMapper, times(1)).selectList(any(Wrapper.class));
        }

        @Test
        public void getBooks_ReturnEmptyPage_IfNoFoundBooks(){
            List<Book> emptyBooksList = new ArrayList<>();

            doReturn(emptyBooksList).when(bookMapper).selectList(any(Wrapper.class));

            BookQueryCriteria bookQueryCriteria = new BookQueryCriteria();
            ResponseEntity<Page<Book>> empList = bookService.getBooks(bookQueryCriteria);

            assertEquals(0, empList.getBody().getRecords().size());
            assertEquals(HttpStatus.OK.value(),empList.getStatusCode().value());
            verify(bookMapper, times(1)).selectList(any(Wrapper.class));
        }
    }

    @Nested
    @DisplayName("Add Book Method Tests")
    class AddBookTests{
        @Test
        public void addBook_ReturnTheInsertedBook_IfSuccessInSavingTheBook(){
            Book expectedBook = VALID_BOOK;

            doReturn(1).when(bookMapper).insert(any(Book.class));

            ResponseEntity<Book> insertedBook = bookService.addBook(expectedBook);

            assertEquals(expectedBook.getName(), insertedBook.getBody().getName());
            assertEquals(expectedBook.getAuthorName(), insertedBook.getBody().getAuthorName());
            assertEquals(expectedBook.getGenre(), insertedBook.getBody().getGenre());
            assertEquals(expectedBook.getPrice(), insertedBook.getBody().getPrice());
            assertEquals(HttpStatus.OK.value(),insertedBook.getStatusCode().value());
            verify(bookMapper, times(1)).insert(any(Book.class));
        }

        @Test
        public void addBook_ReturnBookAlreadyExistsException_IfSavingAnExistingBook(){
            Book expectedBook = VALID_BOOK;

            doThrow(DuplicateKeyException.class).when(bookMapper).insert(any(Book.class));

            BookAlreadyExistsException exception = assertThrows(BookAlreadyExistsException.class, () -> {
                bookService.addBook(expectedBook);
            });

            assertEquals("Book creation failed. Book with name ["+expectedBook.getName()+"] already exists.", exception.getMessage());
        }
    }

    @Nested
    @DisplayName("Update Book Method Tests")
    class UpdateBookTests{
        @Test
        public void updateBook_ReturnTheUpdatedBook_IfSuccessInUpdatingTheBook(){
            Book expectedBook = VALID_BOOK;

            doReturn(1).when(bookMapper).update(any(Book.class),any(Wrapper.class));

            ResponseEntity<Book> insertedBook = bookService.updateBook(expectedBook,14L);

            assertEquals(expectedBook.getName(), insertedBook.getBody().getName());
            assertEquals(expectedBook.getAuthorName(), insertedBook.getBody().getAuthorName());
            assertEquals(expectedBook.getGenre(), insertedBook.getBody().getGenre());
            assertEquals(expectedBook.getPrice(), insertedBook.getBody().getPrice());
            assertEquals(HttpStatus.OK.value(),insertedBook.getStatusCode().value());
            verify(bookMapper, times(1)).update(any(Book.class), any(Wrapper.class));
        }

        @Test
        public void updateBook_ReturnBookAlreadyExistsException_IfTheUpdateValueIsAnExistingBook(){
            Book expectedBook = VALID_BOOK;

            doThrow(DuplicateKeyException.class).when(bookMapper).update(any(Book.class),any(Wrapper.class));

            BookAlreadyExistsException exception = assertThrows(BookAlreadyExistsException.class, () -> {
                bookService.updateBook(expectedBook,14L);
            });
            verify(bookMapper, times(1)).update(any(Book.class), any(Wrapper.class));
            assertEquals("Book update failed. Book with name ["+expectedBook.getName()+"] already exists.", exception.getMessage());
        }

        @Test
        public void updateBook_ReturnBookDoesNotExistException_IfUpdatingABookWithExistingRecords(){
            Long bookId = 14L;
            Book expectedBook = VALID_BOOK;
            expectedBook.setId(bookId);
            doReturn(0).when(bookMapper).update(any(Book.class),any(Wrapper.class));

            BookDoesNotExistException exception = assertThrows(BookDoesNotExistException.class, () -> {
                bookService.updateBook(expectedBook,bookId);
            });
            verify(bookMapper, times(1)).update(any(Book.class), any(Wrapper.class));
            assertEquals("Update failed. Book with id ["+expectedBook.getId()+"] does not exist.", exception.getMessage());
        }
    }

    @Nested
    @DisplayName("Delete Book Method Tests")
    class DeleteBookTests{
        @Test
        public void deleteBookById_ReturnSuccessMessage_IfTheBookIsSuccessfullyDeleted(){
            Long bookId = 14L;
            doReturn(1).when(bookMapper).delete(any(Wrapper.class));

            ResponseEntity<String> insertedBook = bookService.deleteBookById(bookId);

            verify(bookMapper, times(1)).delete(any(Wrapper.class));
            assertEquals("Book with id ["+bookId+"] has been deleted.", insertedBook.getBody());
        }

        @Test
        public void deleteBookById_ThrowBookDoesNotExistException_IfDeletingNotExistingBook(){
            Long bookId = 14L;
            doReturn(0).when(bookMapper).delete(any(Wrapper.class));

            BookDoesNotExistException exception = assertThrows(BookDoesNotExistException.class, () -> {
                bookService.deleteBookById(bookId);
            });

            verify(bookMapper, times(1)).delete(any(Wrapper.class));
            assertEquals("Deletion failed. Book with id ["+bookId+"] does not exist.", exception.getMessage());
        }
    }
}
