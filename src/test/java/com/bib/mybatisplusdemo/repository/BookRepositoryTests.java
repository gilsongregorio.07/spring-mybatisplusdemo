package com.bib.mybatisplusdemo.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.bib.mybatisplusdemo.MybatisplusdemoApplication;
import com.bib.mybatisplusdemo.mapper.BookMapper;
import com.bib.mybatisplusdemo.model.Book;
import com.bib.mybatisplusdemo.model.BookQueryCriteria;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static utils.BookUtils.generateValidBookForUpdate;
import static utils.BookUtils.generateValidBooks;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {MybatisplusdemoApplication.class})
@Transactional
public class BookRepositoryTests {
    @Autowired
    private BookMapper bookMapper;
    private Book VALID_BOOK = generateValidBooks();
    private Book UPDATE_VALID_BOOK = generateValidBookForUpdate();

    @Nested
    @DisplayName("Get Books Method Tests")
    @Rollback
    class GetBooksTests {
        @Test
        public void getBooks_ShowTheListOfBooks_IfThereIsBooksInDatabase() {
            bookMapper.insert(VALID_BOOK);
            BookQueryCriteria criteria = new BookQueryCriteria();

            LambdaQueryWrapper<Book> wrapper = new LambdaQueryWrapper<>();
            wrapper.like(criteria.getBookName() != "" && criteria.getBookName() != null,Book::getName,criteria.getBookName());

            List<Book> bookFound = bookMapper.selectList(wrapper);
            assertNotEquals(0,bookFound.size());
        }
        @Test
        public void getBooks_ReturnFilteredList_IfTheFilteredBookIsFound() {
            bookMapper.insert(VALID_BOOK);
            BookQueryCriteria criteria = new BookQueryCriteria();
            criteria.setBookName("This book should be found");

            LambdaQueryWrapper<Book> wrapper = new LambdaQueryWrapper<>();
            wrapper.like(criteria.getBookName() != "" && criteria.getBookName() != null,Book::getName,criteria.getBookName());

            List<Book> bookFound = bookMapper.selectList(wrapper);
            assertEquals(1,bookFound.size());
        }
        @Test
        public void getBooks_ReturnEmptyList_IfTheFilteredBookIsBotFound() {
            bookMapper.insert(VALID_BOOK);
            BookQueryCriteria criteria = new BookQueryCriteria();
            criteria.setBookName("This book should not be found");

            LambdaQueryWrapper<Book> wrapper = new LambdaQueryWrapper<>();
            wrapper.like(criteria.getBookName() != "" && criteria.getBookName() != null,Book::getName,criteria.getBookName());

            List<Book> bookFound = bookMapper.selectList(wrapper);
            assertEquals(0,bookFound.size());
        }
    }

    @Nested
    @DisplayName("Insert Books Method Tests")
    @Rollback
    class InsertBooksTests {
        @Test
        public void insertBook_ReturnTheInsertedBook_IfTheBookSavedSuccessfully() {
            Book insertedBook = VALID_BOOK;
            bookMapper.insert(insertedBook);

            LambdaQueryWrapper<Book> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Book::getId, insertedBook.getId());

            Book bookFound = bookMapper.selectOne(wrapper);

            assertEquals(insertedBook.getName(), bookFound.getName());
            assertEquals(insertedBook.getAuthorName(), bookFound.getAuthorName());
            assertEquals(insertedBook.getGenre(), bookFound.getGenre());
            assertEquals(insertedBook.getPrice(), bookFound.getPrice());
        }
    }

    @Nested
    @DisplayName("Update Books Method Tests")
    @Rollback
    class UpdateBooksTests {
        @Test
        public void updateBook_ReturnTheUpdatedBook_IfTheBookUpdatedSuccessfully() {
            Book insertedBook = VALID_BOOK;
            bookMapper.insert(insertedBook);

            LambdaQueryWrapper<Book> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Book::getId, insertedBook.getId());

            Book updatedBook = UPDATE_VALID_BOOK;
            updatedBook.setId(insertedBook.getId());
            bookMapper.update(updatedBook, wrapper);

            Book bookFound = bookMapper.selectOne(wrapper);

            assertNotEquals(insertedBook.getName(), bookFound.getName());
            assertNotEquals(insertedBook.getAuthorName(), bookFound.getAuthorName());
            assertNotEquals(insertedBook.getGenre(), bookFound.getGenre());
            assertNotEquals(insertedBook.getPrice(), bookFound.getPrice());
        }

        @Test
        public void updateBook_ReturnZeroRowAffected_IfNoFoundBookToUpdate() {
            Book insertedBook = VALID_BOOK;
            bookMapper.insert(insertedBook);

            LambdaQueryWrapper<Book> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Book::getId, insertedBook.getId());

            Book updatedBook = UPDATE_VALID_BOOK;
            updatedBook.setId(insertedBook.getId());
            bookMapper.update(updatedBook, wrapper);

            Book bookFound = bookMapper.selectOne(wrapper);

            assertNotEquals(insertedBook.getName(), bookFound.getName());
            assertNotEquals(insertedBook.getAuthorName(), bookFound.getAuthorName());
            assertNotEquals(insertedBook.getGenre(), bookFound.getGenre());
            assertNotEquals(insertedBook.getPrice(), bookFound.getPrice());
        }
    }

    @Nested
    @DisplayName("Delete Books Method Tests")
    @Rollback
    class DeleteBooksTests {
        @Test
        public void deleteBook_ReturnOneRowAffected_IfTheBookDeletedSuccessfully() {
            bookMapper.insert(VALID_BOOK);

            LambdaQueryWrapper<Book> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Book::getId, VALID_BOOK.getId());

            Integer rowAffected = bookMapper.delete(wrapper);

            assertEquals(1, rowAffected);
        }

        @Test
        public void deleteBook_ReturnZeroRowAffected_IfDeletingANonExistingBook() {
            LambdaQueryWrapper<Book> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(Book::getId, 0);

            Integer rowAffected = bookMapper.delete(wrapper);

            assertEquals(0, rowAffected);
        }
    }
}
