package utils;

import com.bib.mybatisplusdemo.model.Book;

import java.util.ArrayList;
import java.util.List;

public class BookUtils {
    public static Book generateValidBooks() {
        Book book = Book.builder()
                .name("This book should be found")
                .genre("Test Genre")
                .authorName("Robert Martin")
                .price(300.30).build();
        return book;
    }

    public static Book generateValidBookForUpdate() {
        Book book = Book.builder()
                .name("Updated book name")
                .genre("Updated Genre")
                .authorName("Updated Author")
                .price(9000.9).build();
        return book;
    }

    public static Book generateInvalidBooks() {
        Book book = Book.builder()
                .name("")
                .genre("")
                .authorName("")
                .price(-300.30).build();
        return book;
    }

    public static List<Book> generateListOfBooks() {
        Book book1 = Book.builder()
                .name("Clean Coding")
                .genre("Programming")
                .authorName("Robert Martin")
                .price(500.00).build();

        Book book2 = Book.builder()
                .name("Java Fundamentals")
                .genre("Programming")
                .authorName("Jorge Ivan Medina")
                .price(50.50).build();

        List<Book> bookList = new ArrayList<>();
        bookList.add(book1);
        bookList.add(book2);

        return bookList;
    }
}
