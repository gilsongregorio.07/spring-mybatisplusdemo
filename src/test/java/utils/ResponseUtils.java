package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class ResponseUtils {

    public static Map<String, Object> convertStringResponseToMap(String json) throws JsonProcessingException {
        Map<String, Object> response = new ObjectMapper().readValue(json, HashMap.class);
        return response;
    }


}
