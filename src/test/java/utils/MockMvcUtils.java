package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public class MockMvcUtils {

    public static MvcResult performRequest(MockMvc mockMvc, Object json, String endpoints, HttpMethod method) throws Exception {
        MockHttpServletRequestBuilder requestBuilder = null;
        switch (method){
            case GET:
                requestBuilder = MockMvcRequestBuilders.get(endpoints);
                break;
            case POST:
                requestBuilder = MockMvcRequestBuilders.post(endpoints);
                break;
            case PUT:
                requestBuilder = MockMvcRequestBuilders.put(endpoints);
                break;
            case DELETE:
                requestBuilder = MockMvcRequestBuilders.delete(endpoints);
                break;
            default:
                throw new IllegalArgumentException("You have used an invalid HTTP Method");
        }
        return mockMvc.perform(requestBuilder
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(json)))
                .andReturn();
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            return null;
        }
    }
}
