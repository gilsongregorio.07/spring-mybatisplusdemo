package com.bib.mybatisplusdemo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bib.mybatisplusdemo.advice.BookAlreadyExistsException;
import com.bib.mybatisplusdemo.advice.BookDoesNotExistException;
import com.bib.mybatisplusdemo.mapper.BookMapper;
import com.bib.mybatisplusdemo.model.Book;
import com.bib.mybatisplusdemo.model.BookQueryCriteria;
import com.bib.mybatisplusdemo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.bib.mybatisplusdemo.utils.PageUtils.toPage;

/**
 * @author ef-mark.g
 */
@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookMapper bookMapper;

    @Override
    public ResponseEntity<Page<Book>> getBooks(BookQueryCriteria criteria) {
        LambdaQueryWrapper<Book> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(criteria.getBookName() != "" && criteria.getBookName() != null,Book::getName,criteria.getBookName());

        List<Book> books = bookMapper.selectList(wrapper);

        Page<Book> page = new Page<>(criteria.getPage(), criteria.getSize());
        page.setRecords(toPage(criteria.getPage()-1, criteria.getSize(),books));
        page.setTotal(books.size());
        return new ResponseEntity<>(page, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Book> addBook(Book book) {
        try {
            bookMapper.insert(book);
        }
        catch (DuplicateKeyException e){
            throw new BookAlreadyExistsException(String.format("Book creation failed. Book with name ["+book.getName()+"] already exists."));
        }
        return ResponseEntity.ok(book);
    }

    @Override
    public ResponseEntity<Book> updateBook(Book book, Long id) {
        LambdaQueryWrapper<Book> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Book::getId, id);
        int rowCount = 0;
        try {
            rowCount = bookMapper.update(book, wrapper);
        }
        catch (DuplicateKeyException e){
            throw new BookAlreadyExistsException(String.format("Book update failed. Book with name [%s] already exists.", book.getName()));
        }
        if (rowCount <= 0) {
            throw new BookDoesNotExistException(String.format("Update failed. Book with id [%d] does not exist.", id));
        }
        book.setId(id);
        return new ResponseEntity<>(book, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> deleteBookById(Long id) {
        LambdaQueryWrapper<Book> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Book::getId, id);
        int rowCount = bookMapper.delete(wrapper);
        if(rowCount <= 0) {
            throw new BookDoesNotExistException(String.format("Deletion failed. Book with id [%d] does not exist.", id));
        }
        return new ResponseEntity<>(String.format("Book with id [%d] has been deleted.", id), HttpStatus.OK);
    }
}
