package com.bib.mybatisplusdemo.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bib.mybatisplusdemo.model.Book;
import com.bib.mybatisplusdemo.model.BookQueryCriteria;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author ef-mark.g
 */
public interface BookService {
    ResponseEntity<Page<Book>> getBooks(BookQueryCriteria criteria);

    ResponseEntity<Book> addBook(@RequestBody Book user);

    ResponseEntity<Book> updateBook(@RequestBody Book book, @PathVariable Long id);

    ResponseEntity<String> deleteBookById(@PathVariable Long id);
}
