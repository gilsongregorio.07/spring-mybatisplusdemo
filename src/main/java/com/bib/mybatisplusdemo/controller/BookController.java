package com.bib.mybatisplusdemo.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bib.mybatisplusdemo.model.Book;
import com.bib.mybatisplusdemo.model.BookQueryCriteria;
import com.bib.mybatisplusdemo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/books")
public class BookController {

    @Autowired
    BookService bookService;

    @GetMapping("/list")
    public  ResponseEntity<Page<Book>> getBooks(@RequestBody BookQueryCriteria criteria){

        return bookService.getBooks(criteria);
    }

    @PostMapping("/save")
    public ResponseEntity<Book> insertBook(@Valid @RequestBody Book book){

        return bookService.addBook(book);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Book> updateBook(@Valid @RequestBody Book book,  @PathVariable Long id){
        return bookService.updateBook(book,id);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteBook(@PathVariable Long id){
        return bookService.deleteBookById(id);
    }
}
