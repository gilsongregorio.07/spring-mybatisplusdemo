package com.bib.mybatisplusdemo.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Data
@TableName("books")
@Builder
public class Book {
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String genre;

    @TableField("author_name")
    @NotBlank
    private String authorName;

    @Positive
    private Double price;
}
