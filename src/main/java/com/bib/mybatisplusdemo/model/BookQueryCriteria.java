package com.bib.mybatisplusdemo.model;

import lombok.Data;

@Data
public class BookQueryCriteria {
    Integer page = 1;
    Integer size = 10;
    String bookName;
}
