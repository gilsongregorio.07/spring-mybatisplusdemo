package com.bib.mybatisplusdemo.advice;

public class BookDoesNotExistException extends RuntimeException{

    public BookDoesNotExistException(String message){
        super(message);
    }
}
